# vagrant-virtualbox

This is repository has `Vagrantfile` vagrant configuration file. We can use `vagrant` tool to create `Ubuntu` OS vm using `VirtualBox` provider

Also [webapp] (https://bitbucket.org/arunbagul/webapp-playbook/src/master/) python flask based webapp will be installed using `ansible` provisioner of `vagrant`

* Steps

1) Install vagrant and VirtualBox on your Host

2) Get `Ubuntu` OS base box for Vagrant

  `$ vagrant init ubuntu/trusty64`

4) Clone this Git repository and playbook

  `$ git clone https://arunbagul@bitbucket.org/arunbagul/vagrant-virtualbox.git`
  
  `$ cd vagrant-virtualbox`

   `$ git clone git clone https://arunbagul@bitbucket.org/arunbagul/webapp-playbook.git`

3) Start VM/Instance

  `$ vagrant up`

4) How to run playbook manually

  `$ vagrant provision`
  
